# foaf:Organization a owl:Class extends [foaf:Agent](/foaf/0.1/Agent)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|The Organization class represents a kind of Agent corresponding to social instititutions such as companies, societies etc.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[foaf:logo](/foaf/0.1/logo)|Link to logo of organization|0:1|IRI|
|[prov:legalName](/ns/prov/legalName)|The legal name of the organization|0:1|xsd:string|
|[foaf:phone](/foaf/0.1/phone)|The phone number of the organization|0:1|xsd:string|
|[foaf:homepage](/foaf/0.1/homepage)|Homepage of the organization|0:1|xsd:string|
|[foaf:based_near](/foaf/0.1/based_near)|Location to which the organization is based near|0:1|xsd:string|
