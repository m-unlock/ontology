# logicalPath a ObjectProperty

## Domain

definition: Logical path to folder as stored in a system<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Logical path to folder as stored in a system<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: Logical path to folder as stored in a system<br>
[jerm:Project](/ontology/JERMOntology/Project)

definition: Logical path to folder as stored in a system<br>
[jerm:process](/ontology/JERMOntology/process)

definition: Logical path to folder as stored in a system<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: Logical path to folder as stored in a system<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: Logical path to folder as stored in a system<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: Logical path to folder as stored in a system<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: Logical path to folder as stored in a system<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

definition: Logical path to folder as stored in a system<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: Logical path to folder as stored in a system<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: Logical path to folder as stored in a system<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: Logical path to folder as stored in a system<br>
[jerm:Study](/ontology/JERMOntology/Study)

definition: Logical path to folder as stored in a system<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

xsd:string

## Annotations


