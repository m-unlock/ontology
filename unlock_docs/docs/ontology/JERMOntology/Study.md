# jerm:Study a owl:Class extends [jerm:process](/ontology/JERMOntology/process)

## Subclasses

## Annotations

|||
|-----|-----|
|ppeo:hasExactSynonym|experiment|
|<http://www.w3.org/2004/02/skos/core#exactMatch>|<a href="http://jermontology.org/ontology/JERMOntology#Study">http://jermontology.org/ontology/JERMOntology#Study</a>|
|rdfs:comment|A particular biological hypothesis, which you are planning to test in various ways, using various techniques, which could be experimental, informatics, modelling, or a mixture.<br><br>Comparison of S. solfataricus grown at 70 and 80 degrees|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[contact](/ontology/contact)|Main contact person for the study performed|0:1|[foaf:Person](/foaf/0.1/Person)|
|[ownedBy](/ontology/ownedBy)|The person who owns or performs the experiment, making it the owner|0:N|[foaf:Person](/foaf/0.1/Person)|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[observationUnit](/ontology/observationUnit)|Link to the unit that is observed|1:N|[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)|
|[jerm:description](/ontology/JERMOntology/description)|Description of a given section|1:1|xsd:string|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
