# version a ObjectProperty

## Domain

definition: The version of the software as typically reported with the -version parameter<br>
[prov:SoftwareAgent](/ns/prov/SoftwareAgent)

## Range

xsd:string

## Annotations


