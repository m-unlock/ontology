# observationUnit a ObjectProperty

## Domain

definition: Link to the unit that is observed<br>
[jerm:Study](/ontology/JERMOntology/Study)

## Range

[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

## Annotations


