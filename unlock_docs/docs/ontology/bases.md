# bases a ObjectProperty

## Domain

definition: Number of bases in the sequence file<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: Number of bases in the sequence file<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: Number of bases in the sequence file<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:long

## Annotations


