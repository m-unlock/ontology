# reference a ObjectProperty

## Domain

definition: Reference dataset often used in omics experiments<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Reference dataset often used in omics experiments<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: Reference dataset often used in omics experiments<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: Reference dataset often used in omics experiments<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: Reference dataset often used in omics experiments<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: Reference dataset often used in omics experiments<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: Reference dataset often used in omics experiments<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: Reference dataset often used in omics experiments<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

## Annotations


