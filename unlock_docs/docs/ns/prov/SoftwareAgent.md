# prov:SoftwareAgent a owl:Class extends [foaf:Agent](/foaf/0.1/Agent)

## Subclasses

## Annotations

|||
|-----|-----|
|rdfs:comment|A software agent is running software.<br><br>Software that was used for analyses, the belonging running parameters and information about the software tool|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[commitId](/ontology/commitId)|The String that uniquely identifies the last commit of the code used.|0:1|xsd:string|
|[codeRepository](/ontology/codeRepository)|The (GIT) repository containing the code of the tool|0:1|IRI|
|[version](/ontology/version)|The version of the software as typically reported with the -version parameter|1:1|xsd:string|
|[softwareName](/ontology/softwareName)|Name of the tool|1:1|xsd:string|
|[foaf:homepage](/foaf/0.1/homepage)|Website home page of the software used|0:1|xsd:string|
