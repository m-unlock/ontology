# file a ObjectProperty

## Domain

definition: Data files belonging to this assay<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Data files belonging to this assay<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: Data files belonging to this assay<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: Data files belonging to this assay<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: Data files belonging to this assay<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: Data files belonging to this assay<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: Data files belonging to this assay<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: Data files belonging to this assay<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|Pointing to a file object|

