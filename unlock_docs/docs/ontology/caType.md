# caType a ObjectProperty

## Domain

definition: Computational assay type<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

## Range

[ComputationalAssayType](/ontology/ComputationalAssayType)

## Annotations


