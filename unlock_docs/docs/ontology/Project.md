# Project a owl:Class extends [jerm:Information_entity](/ontology/JERMOntology/Information_entity)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#exactMatch>|<a href="http://jermontology.org/ontology/JERMOntology#Project">http://jermontology.org/ontology/JERMOntology#Project</a>|
|rdfs:comment|A research programme or a grant-funded programme funding the investigations.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[contact](/ontology/contact)|The person who owns or performs the experiment, making it the owner|0:N|[foaf:Person](/foaf/0.1/Person)|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
|[description](/ontology/description)|Description of a given section|1:1|xsd:string|
|[investigation](/ontology/investigation)|Investigations linked|1:N|[jerm:Investigation](/ontology/JERMOntology/Investigation)|
