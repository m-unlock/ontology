# reverseBarcode a ObjectProperty

## Domain

definition: Reverse barcode sequence for an assay for a given library<br>
[AmpliconAssay](/ontology/AmpliconAssay)

## Range

xsd:string

## Annotations


