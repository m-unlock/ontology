# title a ObjectProperty

## Domain

definition: Title of a given section<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Title of a given section<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: Title of a given section<br>
[jerm:Project](/ontology/JERMOntology/Project)

definition: Title of a given section<br>
[jerm:process](/ontology/JERMOntology/process)

definition: Title of a given section<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: Title of a given section<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: Title of a given section<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: Title of a given section<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: Title of a given section<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

definition: Title of a given section<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: Title of a given section<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: Title of a given section<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: Title of a given section<br>
[jerm:Study](/ontology/JERMOntology/Study)

definition: Title of a given section<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

xsd:string

## Annotations


