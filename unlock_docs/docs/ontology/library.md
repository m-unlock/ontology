# library a ObjectProperty

## Domain

definition: Amplicon library associated to this amplicon assay<br>
[AmpliconAssay](/ontology/AmpliconAssay)

## Range

[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

## Annotations


