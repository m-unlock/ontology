# altitude a ObjectProperty

## Domain

definition: Altitude of the location from which the sample was collected in meters above or below nominal sea level<br>
[Origin](/ontology/Origin)

## Range

xsd:double

## Annotations


