# readLength a ObjectProperty

## Domain

definition: Read length<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: Read length<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: Read length<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:long

## Annotations


