# generatedBy a ObjectProperty

## Domain

definition: The company that generated the data<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

[foaf:Organization](/foaf/0.1/Organization)

## Annotations


