# sop a ObjectProperty

## Domain

definition: SOPs used to generate the assay<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: SOPs used to generate the assay<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: SOPs used to generate the assay<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: SOPs used to generate the assay<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: SOPs used to generate the assay<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: SOPs used to retrieve the sample<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

definition: SOPs used to generate the assay<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: SOPs used to generate the assay<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: SOPs used to generate the assay<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

xsd:anyURI

## Annotations


