# bibo:prefixName a ObjectProperty

## Domain

definition: The prefix of a name<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:string

## Annotations


