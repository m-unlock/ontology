# jerm:Sample a owl:Class extends [jerm:process](/ontology/JERMOntology/process)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#definition>|Information about the sample|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[assay](/ontology/assay)||0:N|[jerm:Assay](/ontology/JERMOntology/Assay)|
|[ownedBy](/ontology/ownedBy)|The person who owns or performs the experiment, making it the owner|0:N|[foaf:Person](/foaf/0.1/Person)|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[jerm:description](/ontology/JERMOntology/description)|Description of a given section|1:1|xsd:string|
|[collectionDate](/ontology/collectionDate)|The date and time on which the specimen was collected.|0:1|xsd:DateTime|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[sop](/ontology/sop)|SOPs used to retrieve the sample|0:N|IRI|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
|[origin](/ontology/origin)|Origin of the sample, a bioreactor, animal, soil, etc...|1:1|[Origin](/ontology/Origin)|
|[note](/ontology/note)|A note on the sample|0:N|xsd:string|
|[collectedBy](/ontology/collectedBy)|The persons or institute who collected the specimen|0:N|[foaf:Agent](/foaf/0.1/Agent)|
