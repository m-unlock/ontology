# codeRepository a ObjectProperty

## Domain

definition: The (GIT) repository containing the code of the tool<br>
[prov:SoftwareAgent](/ns/prov/SoftwareAgent)

## Range

xsd:anyURI

## Annotations


