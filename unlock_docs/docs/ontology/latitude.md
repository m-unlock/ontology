# latitude a ObjectProperty

## Domain

definition: latitude given in degrees. Positive is to the north, negative is to the south.<br>
[Origin](/ontology/Origin)

## Range

xsd:double

## Annotations


