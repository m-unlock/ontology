#!/bin/bash
#============================================================================
#title          :Unlock Ontology
#description    :Unlock ontology used within iRODS
#author         :Jasper Koehorst
#date           :2019
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

wget -nc http://download.systemsbiology.nl/empusa/EmpusaCodeGen.jar -O $DIR/EmpusaCodeGen.jar

# Clean up docs
rm -rf $DIR/unlock_docs/docs/

java -jar $DIR/EmpusaCodeGen.jar -i $DIR/UNLOCK.ttl -doc $DIR/unlock_docs -o $DIR/unlockAPI -excludeBaseFiles

$DIR/unlockAPI/install.sh