# jerm:description a ObjectProperty

## Domain

definition: Description of a given section<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Description of a given section<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: Description of a given section<br>
[jerm:process](/ontology/JERMOntology/process)

definition: Description of a given section<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: Description of a given section<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: Description of a given section<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: Description of a given section<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: Description of a given section<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

definition: Description of a given section<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: Description of a given section<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: Description of a given section<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: Description of a given section<br>
[jerm:Study](/ontology/JERMOntology/Study)

definition: Description of a given section<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

xsd:string

## Annotations


