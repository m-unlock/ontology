# contact a ObjectProperty

## Domain

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:Project](/ontology/JERMOntology/Project)

[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: Main contact person for the study performed<br>
[jerm:Study](/ontology/JERMOntology/Study)

## Range

[foaf:Person](/foaf/0.1/Person)

## Annotations


