# bibo:suffixName a ObjectProperty

## Domain

definition: The suffix of a name<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:string

## Annotations


