# physicalPath a ObjectProperty

## Domain

definition: Physical Path to folder as stored in a system<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: Physical Path to folder as stored in a system<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: Physical Path to folder as stored in a system<br>
[jerm:Project](/ontology/JERMOntology/Project)

definition: Physical Path to folder as stored in a system<br>
[jerm:process](/ontology/JERMOntology/process)

definition: Physical Path to folder as stored in a system<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: Physical Path to folder as stored in a system<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: Physical Path to folder as stored in a system<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: Physical Path to folder as stored in a system<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: Physical Path to folder as stored in a system<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

definition: Physical Path to folder as stored in a system<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: Physical Path to folder as stored in a system<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: Physical Path to folder as stored in a system<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: Physical Path to folder as stored in a system<br>
[jerm:Study](/ontology/JERMOntology/Study)

definition: Physical Path to folder as stored in a system<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

xsd:string

## Annotations


