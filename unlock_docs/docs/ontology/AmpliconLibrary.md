# AmpliconLibrary a skos:Concept, [AssayType](/ontology/AssayType)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|A library containing multiple amplicon assays|

