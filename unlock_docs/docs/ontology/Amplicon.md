# Amplicon a skos:Concept, [AssayType](/ontology/AssayType)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|An assay based on amplicon sequencing. This is assay is a demultiplexed amplicon dataset.|

