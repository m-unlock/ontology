# foaf:Person a owl:Class extends [foaf:Agent](/foaf/0.1/Agent)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#definition>|Information related to a Person|
|rdfs:comment|The Person class represents people. Something is a Person if it is a person. We don't nitpic about whether they're alive, dead, real, or imaginary. The Person class is a sub-class of the Agent class, since all people are considered 'agents' in FOAF.|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[foaf:phone](/foaf/0.1/phone)|The phone number of the person|0:1|xsd:string|
|[orcid](/ontology/orcid)|The unique identifier to identify the scientist|0:1|IRI|
|[bibo:suffixName](/ontology/bibo/suffixName)|The suffix of a name|0:1|xsd:string|
|[foaf:givenName](/foaf/0.1/givenName)|The given name of a person|0:1|xsd:string|
|[organization](/ontology/organization)|To which organization(s) does this person belongs to?|0:N|[foaf:Organization](/foaf/0.1/Organization)|
|[foaf:surName](/foaf/0.1/surName)|The first name of the person|1:1|xsd:string|
|[bibo:prefixName](/ontology/bibo/prefixName)|The prefix of a name|0:1|xsd:string|
|[foaf:depiction](/foaf/0.1/depiction)|Image of the person use link to image|0:1|IRI|
|[bibo:localityName](/ontology/bibo/localityName)|Used to name the locality of a publisher, an author, etc.|0:1|xsd:string|
|[foaf:title](/foaf/0.1/title)|Title of the person  (Mr, Mrs, Ms, Dr. etc)|0:1|xsd:string|
|[foaf:firstName](/foaf/0.1/firstName)|The first name of the person|1:1|xsd:string|
|[foaf:homepage](/foaf/0.1/homepage)|Homepage of the person (for example link to linkedin or research gate)|0:1|xsd:string|
|[foaf:mbox](/foaf/0.1/mbox)|mail address use  URI scheme (see RFC 2368).|1:1|IRI|
|[department](/ontology/department)|To which department the person belongs to|0:1|xsd:string|
