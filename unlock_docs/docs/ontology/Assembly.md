# Assembly a skos:Concept, [ComputationalAssayType](/ontology/ComputationalAssayType)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|Sequence assembly of raw reads coming from sequencing experiment|
|<http://www.w3.org/2004/02/skos/core#definition>|Aligning and merging fragments from a longer DNA/RNA sequence in order to reconstruct the original sequence|

