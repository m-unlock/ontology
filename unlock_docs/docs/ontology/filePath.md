# filePath a ObjectProperty

## Domain

definition: String path of the file<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: String path of the file<br>
[ReactorDataSet](/ontology/ReactorDataSet)

definition: String path of the file<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: String path of the file<br>
[RDFDataSet](/ontology/RDFDataSet)

definition: String path of the file<br>
[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

definition: String path of the file<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:string

## Annotations


