# jerm:Asset a owl:Class extends [jerm:Information_entity](/ontology/JERMOntology/Information_entity)

## Subclasses

[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

## Annotations

|||
|-----|-----|
|rdfs:comment|all shareable entities are kinds of Assets|

## Properties

