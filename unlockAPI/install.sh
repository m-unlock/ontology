#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

gradle build -b "$DIR/build.gradle" 

cp $DIR/build/libs/*.jar $DIR/

mvn install:install-file -Dfile=$DIR/UnlockOntology.jar -DgroupId=nl.munlock -DartifactId=unlockapi -Dversion=1.0.1 -Dpackaging=jar

#Comment: Null pointer issue

