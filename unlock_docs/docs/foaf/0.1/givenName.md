# foaf:givenName a ObjectProperty

## Domain

definition: The given name of a person<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:string

## Annotations


