# identifier a ObjectProperty

## Domain

definition: identifier<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: identifier<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: identifier<br>
[jerm:Project](/ontology/JERMOntology/Project)

definition: identifier<br>
[jerm:process](/ontology/JERMOntology/process)

definition: identifier<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: identifier<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: identifier<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: identifier<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: identifier<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

definition: identifier<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: identifier<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: identifier<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: identifier<br>
[jerm:Study](/ontology/JERMOntology/Study)

definition: identifier<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

xsd:string

## Annotations


