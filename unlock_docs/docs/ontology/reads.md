# reads a ObjectProperty

## Domain

definition: Number of reads<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: Number of reads<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: Number of reads<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:long

## Annotations


