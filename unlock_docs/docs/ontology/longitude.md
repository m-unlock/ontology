# longitude a ObjectProperty

## Domain

definition: longitude given in degrees. Positive is to the east, negative is to the west.<br>
[Origin](/ontology/Origin)

## Range

xsd:double

## Annotations


