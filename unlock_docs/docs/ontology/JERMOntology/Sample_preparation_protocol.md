# jerm:Sample_preparation_protocol a owl:Class extends [jerm:Protocol](/ontology/JERMOntology/Protocol)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|describes the treatments and conditions applied at the time of sampling|
|<http://purl.org/dc/elements/1.1/identifier>|JERM:00146|

## Properties

