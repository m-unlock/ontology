# sample a ObjectProperty

## Domain

definition: Sample belonging to the observed unit<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

## Range

[jerm:Sample](/ontology/JERMOntology/Sample)

## Annotations


