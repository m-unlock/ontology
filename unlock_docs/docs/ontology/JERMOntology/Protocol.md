# jerm:Protocol a owl:Class

## Subclasses

[jerm:SOP](/ontology/JERMOntology/SOP)

[jerm:Sample_preparation_protocol](/ontology/JERMOntology/Sample_preparation_protocol)

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|The specification of a laboratory or computational procedure that contains enough detail to enable scientists to rerun that procedure or at least understand and evaluate the method|
|<http://purl.org/dc/elements/1.1/identifier>|JERM:00145|

## Properties

