# Mapping a skos:Concept, [ComputationalAssayType](/ontology/ComputationalAssayType)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|Placement of a collection of molecular markers onto their respective positions on the genome|
|<http://www.w3.org/2004/02/skos/core#definition>|Gene mapping describes the methods used to identify the locus of a gene and the distances between genes.|

