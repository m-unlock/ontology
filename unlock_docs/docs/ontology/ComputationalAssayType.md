# ComputationalAssayType a skos:Collection extends owl:Class

## Annotations


## skos:member

[Mapping](/ontology/Mapping)

[Reporting](/ontology/Reporting)

[Assembly](/ontology/Assembly)

[Annotation](/ontology/Annotation)

[Classification](/ontology/Classification)

[Filtering](/ontology/Filtering)

[QualityControl](/ontology/QualityControl)

