# link a ObjectProperty

## Domain

definition: Overlapping identifier with the RDF dataset making crosslinking possible<br>
[RDFDataSet](/ontology/RDFDataSet)

## Range

xsd:anyURI

## Annotations


