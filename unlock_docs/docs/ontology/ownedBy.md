# ownedBy a ObjectProperty

## Domain

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:Assay](/ontology/JERMOntology/Assay)

definition: The person who owns or performs the experiment, making it the owner<br>
[RNASeqAssay](/ontology/RNASeqAssay)

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:process](/ontology/JERMOntology/process)

definition: The person who owns or performs the experiment, making it the owner<br>
[DNASeqAssay](/ontology/DNASeqAssay)

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:informatics_analysis](/ontology/JERMOntology/informatics_analysis)

definition: The person who owns or performs the experiment, making it the owner<br>
[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

definition: The person who owns or performs the experiment, making it the owner<br>
[AmpliconAssay](/ontology/AmpliconAssay)

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

definition: The person who owns or performs the experiment, making it the owner<br>
[AmpliconLibraryAssay](/ontology/AmpliconLibraryAssay)

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:Investigation](/ontology/JERMOntology/Investigation)

definition: The person who owns or performs the experiment, making it the owner<br>
[SequencingAssay](/ontology/SequencingAssay)

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:Study](/ontology/JERMOntology/Study)

definition: The person who owns or performs the experiment, making it the owner<br>
[jerm:experimental_assay](/ontology/JERMOntology/experimental_assay)

## Range

[foaf:Person](/foaf/0.1/Person)

## Annotations


