# commitId a ObjectProperty

## Domain

definition: The String that uniquely identifies the last commit of the code used.<br>
[prov:SoftwareAgent](/ns/prov/SoftwareAgent)

## Range

xsd:string

## Annotations


