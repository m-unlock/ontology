# base64 a ObjectProperty

## Domain

definition: Base 64 checksum<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: Base 64 checksum<br>
[ReactorDataSet](/ontology/ReactorDataSet)

definition: Base 64 checksum<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: Base 64 checksum<br>
[RDFDataSet](/ontology/RDFDataSet)

definition: Base 64 checksum<br>
[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

definition: Base 64 checksum<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:string

## Annotations


