# foaf:based_near a ObjectProperty

## Domain

definition: Location to which the organization is based near<br>
[foaf:Organization](/foaf/0.1/Organization)

## Range

xsd:string

## Annotations


