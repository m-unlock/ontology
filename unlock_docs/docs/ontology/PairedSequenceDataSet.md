# PairedSequenceDataSet a owl:Class extends [SequenceDataSet](/ontology/SequenceDataSet)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[seqPlatform](/ontology/seqPlatform)|The sequencing platform|0:1|[SequencingPlatform](/ontology/SequencingPlatform)|
|[readLength](/ontology/readLength)|Read length|1:1|xsd:Long|
|[readOrientation](/ontology/readOrientation)|The read orientation of the sequences|0:1|[ReadOrientation](/ontology/ReadOrientation)|
|[bases](/ontology/bases)|Number of bases in the sequence file|1:1|xsd:Long|
|[fileType](/ontology/fileType)|File type|1:1|[FileType](/ontology/FileType)|
|[sha256](/ontology/sha256)|SHA-256 checksum|1:1|xsd:string|
|[fileName](/ontology/fileName)|String name of the file|1:1|xsd:string|
|[base64](/ontology/base64)|Base 64 checksum|1:1|xsd:string|
|[paired](/ontology/paired)|Paired with another data set for example paired-end sequencing|1:1|[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)|
|[reads](/ontology/reads)|Number of reads|1:1|xsd:Long|
|[filePath](/ontology/filePath)|String path of the file|1:1|xsd:string|
