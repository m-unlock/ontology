# SequencingPlatform a skos:Collection extends owl:Class

## Annotations


## skos:member

[PacBio](/ontology/PacBio)

[Illumina](/ontology/Illumina)

[NextGen](/ontology/NextGen)

[SOLID](/ontology/SOLID)

[x454](/ontology/x454)

[NanoPore](/ontology/NanoPore)

