# Process a owl:Class

## Subclasses

[jerm:Assay](/ontology/JERMOntology/Assay)

[ppeo:observation_unit](/ppeo/PPEO.owl/observation_unit)

[jerm:Sample](/ontology/JERMOntology/Sample)

[jerm:Investigation](/ontology/JERMOntology/Investigation)

[jerm:Study](/ontology/JERMOntology/Study)

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[ownedBy](/ontology/ownedBy)|The person who owns or performs the experiment, making it the owner|0:N|[foaf:Person](/foaf/0.1/Person)|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[jerm:description](/ontology/JERMOntology/description)|Description of a given section|1:1|xsd:string|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
