# fileType a ObjectProperty

## Domain

definition: File type<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: File type<br>
[ReactorDataSet](/ontology/ReactorDataSet)

definition: File type<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: File type<br>
[RDFDataSet](/ontology/RDFDataSet)

definition: File type<br>
[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

definition: File type<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

[FileType](/ontology/FileType)

## Annotations


