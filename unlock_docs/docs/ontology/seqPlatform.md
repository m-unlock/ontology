# seqPlatform a ObjectProperty

## Domain

definition: The sequencing platform<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: The sequencing platform<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: The sequencing platform<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

[SequencingPlatform](/ontology/SequencingPlatform)

## Annotations


