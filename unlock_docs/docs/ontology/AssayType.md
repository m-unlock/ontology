# AssayType a skos:Collection extends owl:Class

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|An assay type describes the type of experiment being performed|
|rdfs:comment|The assay_type hierarchy allows scientists to categorise their experimental assays, informatics analyses and computational models|

## skos:member

[Offgas](/ontology/Offgas)

[AmpliconLibrary](/ontology/AmpliconLibrary)

[DNAsequencing](/ontology/DNAsequencing)

[HPLC](/ontology/HPLC)

[RNAsequencing](/ontology/RNAsequencing)

[Amplicon](/ontology/Amplicon)

