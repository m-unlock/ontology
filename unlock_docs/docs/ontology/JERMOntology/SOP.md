# jerm:SOP a owl:Class extends [jerm:Protocol](/ontology/JERMOntology/Protocol)

## Subclasses

## Annotations

|||
|-----|-----|
|<http://purl.org/dc/elements/1.1/description>|Standard operating procedures and protocols - experimental methods that have been agreed and standardised across a project or organisation|
|<http://purl.org/dc/elements/1.1/identifier>|JERM:00015|

## Properties

