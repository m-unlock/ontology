# Standards

The ontology / ISA standard that is used in Unlock can be found in the ontology file.

Full documentation of the generated ontology can be found here https://m-unlock.gitlab.io


# Ontologies

Ontologies that could become useful for the Unlock project:

 - https://www.w3.org/wiki/Good_Ontologies
 - ... FOAF / DCTerms / ... 
 - PROV
 - DCAT (Fair Data Point)? Open air / 
 - EDAM
 - ISA-TAB https://isa-specs.readthedocs.io/en/latest/isatab.html 

Explanation: https://docs.seek4science.org/help/isa-best-practice.html

# Project / Investigation / Study / Assay information

The XLSX has been replaced by an ontology in this repository.
