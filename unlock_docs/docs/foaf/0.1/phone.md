# foaf:phone a ObjectProperty

## Domain

definition: The phone number of the organization<br>
[foaf:Organization](/foaf/0.1/Organization)

definition: The phone number of the person<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:string

## Annotations


