# fileName a ObjectProperty

## Domain

definition: String name of the file<br>
[PairedSequenceDataSet](/ontology/PairedSequenceDataSet)

definition: String name of the file<br>
[ReactorDataSet](/ontology/ReactorDataSet)

definition: String name of the file<br>
[SingleSequenceDataSet](/ontology/SingleSequenceDataSet)

definition: String name of the file<br>
[RDFDataSet](/ontology/RDFDataSet)

definition: String name of the file<br>
[jerm:Data_sample](/ontology/JERMOntology/Data_sample)

definition: String name of the file<br>
[SequenceDataSet](/ontology/SequenceDataSet)

## Range

xsd:string

## Annotations


