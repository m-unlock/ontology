# orcid a ObjectProperty

## Domain

definition: The unique identifier to identify the scientist<br>
[foaf:Person](/foaf/0.1/Person)

## Range

xsd:anyURI

## Annotations


