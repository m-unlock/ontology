# RDFDataSet a owl:Class extends [jerm:Data_sample](/ontology/JERMOntology/Data_sample)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[link](/ontology/link)|Overlapping identifier with the RDF dataset making crosslinking possible|0:N|IRI|
|[fileType](/ontology/fileType)|File type|1:1|[FileType](/ontology/FileType)|
|[sha256](/ontology/sha256)|SHA-256 checksum|1:1|xsd:string|
|[fileName](/ontology/fileName)|String name of the file|1:1|xsd:string|
|[base64](/ontology/base64)|Base 64 checksum|1:1|xsd:string|
|[filePath](/ontology/filePath)|String path of the file|1:1|xsd:string|
