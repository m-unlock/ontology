# collectionDate a ObjectProperty

## Domain

definition: The date and time on which the specimen was collected.<br>
[jerm:Sample](/ontology/JERMOntology/Sample)

## Range

xsd:dateTime

## Annotations


