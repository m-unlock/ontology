# jerm:Data_sample a owl:Class extends [jerm:Asset](/ontology/JERMOntology/Asset)

## Subclasses

[ReactorDataSet](/ontology/ReactorDataSet)

[RDFDataSet](/ontology/RDFDataSet)

[SequenceDataSet](/ontology/SequenceDataSet)

## Annotations

|||
|-----|-----|
|<http://www.w3.org/2004/02/skos/core#definition>|Data encoded in a defined structure.|
|rdfs:comment|Examples include lists, tables, and databases. A dataset may be useful for direct machine processing.|
|rdfs:comment|MIAPPE DATA FILE|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[fileType](/ontology/fileType)|File type|1:1|[FileType](/ontology/FileType)|
|[sha256](/ontology/sha256)|SHA-256 checksum|1:1|xsd:string|
|[fileName](/ontology/fileName)|String name of the file|1:1|xsd:string|
|[base64](/ontology/base64)|Base 64 checksum|1:1|xsd:string|
|[filePath](/ontology/filePath)|String path of the file|1:1|xsd:string|
