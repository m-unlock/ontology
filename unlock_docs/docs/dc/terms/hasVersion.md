# dc:hasVersion a ObjectProperty

## Domain

definition: A related resource that is a version, edition, or adaptation of the described resource.<br>
[dcmitype:Software](/dc/dcmitype/Software)

## Range

xsd:string

## Annotations


