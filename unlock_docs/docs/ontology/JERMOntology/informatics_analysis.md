# jerm:informatics_analysis a owl:Class extends [jerm:Assay](/ontology/JERMOntology/Assay)

## Subclasses

## Annotations

|||
|-----|-----|

## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[end](/ontology/end)|End time|1:1|xsd:DateTime|
|[outputFile](/ontology/outputFile)|Output file|1:N|[jerm:Data_sample](/ontology/JERMOntology/Data_sample)|
|[inputFile](/ontology/inputFile)|Input file|1:N|[jerm:Data_sample](/ontology/JERMOntology/Data_sample)|
|[reference](/ontology/reference)|Reference dataset often used in omics experiments|0:1|[jerm:Data_sample](/ontology/JERMOntology/Data_sample)|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
|[start](/ontology/start)|Starting time|1:1|xsd:DateTime|
|[command](/ontology/command)|Command used for execution|1:1|xsd:string|
|[exitCode](/ontology/exitCode)|Exitcode from the program|1:1|xsd:Integer|
|[ownedBy](/ontology/ownedBy)|The person who owns or performs the experiment, making it the owner|0:N|[foaf:Person](/foaf/0.1/Person)|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[file](/ontology/file)|Data files belonging to this assay|0:N|[jerm:Data_sample](/ontology/JERMOntology/Data_sample)|
|[jerm:description](/ontology/JERMOntology/description)|Description of a given section|1:1|xsd:string|
|[runTime](/ontology/runTime)|Run time in seconds|1:1|xsd:Long|
|[sop](/ontology/sop)|SOPs used to generate the assay|0:N|IRI|
|[software](/ontology/software)|Software information|1:1|[dcmitype:Software](/dc/dcmitype/Software)|
|[caType](/ontology/caType)|Computational assay type|1:1|[ComputationalAssayType](/ontology/ComputationalAssayType)|
