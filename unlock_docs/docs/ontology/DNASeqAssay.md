# DNASeqAssay a owl:Class extends [SequencingAssay](/ontology/SequencingAssay)

## Subclasses

## Annotations


## Properties

|property|description|cardinality|type|
|-----|-----|-----|-----|
|[logicalPath](/ontology/logicalPath)|Logical path to folder as stored in a system|1:1|xsd:string|
|[ownedBy](/ontology/ownedBy)|The person who owns or performs the experiment, making it the owner|0:N|[foaf:Person](/foaf/0.1/Person)|
|[identifier](/ontology/identifier)|identifier|1:1|xsd:string|
|[file](/ontology/file)|Data files belonging to this assay|0:N|[jerm:Data_sample](/ontology/JERMOntology/Data_sample)|
|[jerm:description](/ontology/JERMOntology/description)|Description of a given section|1:1|xsd:string|
|[reference](/ontology/reference)|Reference dataset often used in omics experiments|0:1|[jerm:Data_sample](/ontology/JERMOntology/Data_sample)|
|[title](/ontology/title)|Title of a given section|1:1|xsd:string|
|[sop](/ontology/sop)|SOPs used to generate the assay|0:N|IRI|
|[physicalPath](/ontology/physicalPath)|Physical Path to folder as stored in a system|1:1|xsd:string|
