# organization a ObjectProperty

## Domain

definition: To which organization(s) does this person belongs to?<br>
[foaf:Person](/foaf/0.1/Person)

## Range

[foaf:Organization](/foaf/0.1/Organization)

## Annotations


